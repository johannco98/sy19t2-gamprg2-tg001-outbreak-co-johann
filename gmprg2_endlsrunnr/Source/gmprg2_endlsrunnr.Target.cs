// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class gmprg2_endlsrunnrTarget : TargetRules
{
	public gmprg2_endlsrunnrTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "gmprg2_endlsrunnr" } );
	}
}
