// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "nGameMode.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_ENDLSRUNNR_API AnGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
};
