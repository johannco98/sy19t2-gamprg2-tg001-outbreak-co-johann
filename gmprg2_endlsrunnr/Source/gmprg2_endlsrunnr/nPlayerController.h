// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "nPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class GMPRG2_ENDLSRUNNR_API AnPlayerController : public APlayerController
{
	GENERATED_BODY()
private:
	class ARunCharacter * runCharacter;


protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UFloatingPawnMovement* Movement;

	void MoveForward(float scale);
	void MoveRight(float scale);
};
