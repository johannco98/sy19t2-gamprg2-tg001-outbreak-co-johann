// Fill out your copyright notice in the Description page of Project Settings.

#include "nPlayerController.h"

#include "GameFramework/FloatingPawnMovement.h"
#include "RunCharacter.h"

void AnPlayerController::BeginPlay()
{
	Super::BeginPlay();
	runCharacter = Cast<ARunCharacter>(GetPawn());
}

void AnPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveForward", this, &AnPlayerController::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AnPlayerController::MoveRight);

}

void AnPlayerController::MoveForward(float scale)
{
	const FRotator Rotation = GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	runCharacter->AddMovementInput(Direction, 1);
}

void AnPlayerController::MoveRight(float scale)
{
	const FRotator Rotation = GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);

	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	if (scale != 0)
	{
		if (scale < 0)
		{
			runCharacter->AddMovementInput(Direction, scale);
		}
		else if (scale > 0)
		{
			runCharacter->AddMovementInput(Direction, scale);
		}
	}

}
