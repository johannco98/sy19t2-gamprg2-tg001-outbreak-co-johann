// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class gmprg2_endlsrunnrEditorTarget : TargetRules
{
	public gmprg2_endlsrunnrEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "gmprg2_endlsrunnr" } );
	}
}
