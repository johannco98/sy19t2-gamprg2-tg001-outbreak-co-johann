// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_ENDLSRUNNR_nGameMode_generated_h
#error "nGameMode.generated.h already included, missing '#pragma once' in nGameMode.h"
#endif
#define GMPRG2_ENDLSRUNNR_nGameMode_generated_h

#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_RPC_WRAPPERS
#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAnGameMode(); \
	friend struct Z_Construct_UClass_AnGameMode_Statics; \
public: \
	DECLARE_CLASS(AnGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/gmprg2_endlsrunnr"), NO_API) \
	DECLARE_SERIALIZER(AnGameMode)


#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAnGameMode(); \
	friend struct Z_Construct_UClass_AnGameMode_Statics; \
public: \
	DECLARE_CLASS(AnGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/gmprg2_endlsrunnr"), NO_API) \
	DECLARE_SERIALIZER(AnGameMode)


#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AnGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AnGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AnGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AnGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AnGameMode(AnGameMode&&); \
	NO_API AnGameMode(const AnGameMode&); \
public:


#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AnGameMode(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AnGameMode(AnGameMode&&); \
	NO_API AnGameMode(const AnGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AnGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AnGameMode); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AnGameMode)


#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_PRIVATE_PROPERTY_OFFSET
#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_12_PROLOG
#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_RPC_WRAPPERS \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_INCLASS \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_PRIVATE_PROPERTY_OFFSET \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_INCLASS_NO_PURE_DECLS \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
