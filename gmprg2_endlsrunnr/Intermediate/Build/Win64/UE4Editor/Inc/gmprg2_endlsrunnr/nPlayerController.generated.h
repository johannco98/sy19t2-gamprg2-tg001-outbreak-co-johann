// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GMPRG2_ENDLSRUNNR_nPlayerController_generated_h
#error "nPlayerController.generated.h already included, missing '#pragma once' in nPlayerController.h"
#endif
#define GMPRG2_ENDLSRUNNR_nPlayerController_generated_h

#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_RPC_WRAPPERS
#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAnPlayerController(); \
	friend struct Z_Construct_UClass_AnPlayerController_Statics; \
public: \
	DECLARE_CLASS(AnPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gmprg2_endlsrunnr"), NO_API) \
	DECLARE_SERIALIZER(AnPlayerController)


#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAnPlayerController(); \
	friend struct Z_Construct_UClass_AnPlayerController_Statics; \
public: \
	DECLARE_CLASS(AnPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/gmprg2_endlsrunnr"), NO_API) \
	DECLARE_SERIALIZER(AnPlayerController)


#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AnPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AnPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AnPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AnPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AnPlayerController(AnPlayerController&&); \
	NO_API AnPlayerController(const AnPlayerController&); \
public:


#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AnPlayerController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AnPlayerController(AnPlayerController&&); \
	NO_API AnPlayerController(const AnPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AnPlayerController); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AnPlayerController); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AnPlayerController)


#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Movement() { return STRUCT_OFFSET(AnPlayerController, Movement); }


#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_12_PROLOG
#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_RPC_WRAPPERS \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_INCLASS \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_PRIVATE_PROPERTY_OFFSET \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_INCLASS_NO_PURE_DECLS \
	gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID gmprg2_endlsrunnr_Source_gmprg2_endlsrunnr_nPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
